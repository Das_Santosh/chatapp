//
//  FourthViewController.swift
//  TestExample
//
//  Created by Santosh Kumar on 24/01/20.
//  Copyright © 2020 Santosh Kumar. All rights reserved.
//

import UIKit

class FourthViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    @IBAction func btnPreviousClicked(_ sender: UIButton) {
        
        let viewControllers: [UIViewController] = self.navigationController!.viewControllers as! [FirstViewController];
        
        for aViewController in viewControllers {
            if(aViewController is FirstViewController){
                self.navigationController!.popToViewController(aViewController, animated: true);
            }
        }
    }
    
  

}
