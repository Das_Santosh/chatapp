//
//  SecondViewController.swift
//  TestExample
//
//  Created by Santosh Kumar on 24/01/20.
//  Copyright © 2020 Santosh Kumar. All rights reserved.
//

import UIKit

class SecondViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }


  
    @IBAction func btnNextClicked(_ sender: UIButton) {
        
        let vc = ThirdViewController()
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func btnPreviousClicked(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
}
